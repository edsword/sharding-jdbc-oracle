package com.aecc.sj.oracle.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;

/**
 * mybatis-plus配置，此处各模块需要加入自己的扫描包路径
 * @author Admin
 *
 */
@EnableTransactionManagement
@Configuration
@MapperScan({"com.aecc.sj.oracle.mapper"})
public class MybatisPlusConfig {
	
	@Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }
	
}
