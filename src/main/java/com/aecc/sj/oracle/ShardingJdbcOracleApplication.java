package com.aecc.sj.oracle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShardingJdbcOracleApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShardingJdbcOracleApplication.class, args);
	}

}
