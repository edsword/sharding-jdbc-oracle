package com.aecc.sj.oracle.service;

import com.aecc.sj.oracle.entity.Tabrecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author CodeGenerator
 * @since 2020-11-30
 */
public interface ITabrecordService extends IService<Tabrecord> {

}
