package com.aecc.sj.oracle.service.impl;

import com.aecc.sj.oracle.entity.Goods;
import com.aecc.sj.oracle.mapper.GoodsMapper;
import com.aecc.sj.oracle.service.IGoodsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author CodeGenerator
 * @since 2020-11-11
 */
@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, Goods> implements IGoodsService {

}
