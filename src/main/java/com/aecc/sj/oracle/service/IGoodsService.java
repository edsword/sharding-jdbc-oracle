package com.aecc.sj.oracle.service;

import com.aecc.sj.oracle.entity.Goods;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author CodeGenerator
 * @since 2020-11-11
 */
public interface IGoodsService extends IService<Goods> {

}
