package com.aecc.sj.oracle.service.impl;

import com.aecc.sj.oracle.entity.Tabrecord;
import com.aecc.sj.oracle.mapper.TabrecordMapper;
import com.aecc.sj.oracle.service.ITabrecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author CodeGenerator
 * @since 2020-11-30
 */
@Service
public class TabrecordServiceImpl extends ServiceImpl<TabrecordMapper, Tabrecord> implements ITabrecordService {

}
