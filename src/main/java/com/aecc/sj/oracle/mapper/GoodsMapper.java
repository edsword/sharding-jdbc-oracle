package com.aecc.sj.oracle.mapper;

import com.aecc.sj.oracle.entity.Goods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author CodeGenerator
 * @since 2020-11-11
 */
public interface GoodsMapper extends BaseMapper<Goods> {

}
