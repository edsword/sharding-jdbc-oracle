package com.aecc.sj.oracle.mapper;

import com.aecc.sj.oracle.entity.Tabrecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author CodeGenerator
 * @since 2020-11-30
 */
public interface TabrecordMapper extends BaseMapper<Tabrecord> {

}
