package com.aecc.sj.oracle.controller;


import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aecc.sj.oracle.entity.Tabrecord;
import com.aecc.sj.oracle.service.ITabrecordService;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author CodeGenerator
 * @since 2020-11-30
 */
@RestController
@RequestMapping("/tabrecord")
public class TabrecordController {
	
	@Autowired
	private ITabrecordService tabrecordService;
	
	@GetMapping("/edit")
	public boolean edit() {
		Tabrecord model = new Tabrecord();
		model.setId(IdWorker.getId());
		model.setTname("测试名称");
		model.setAddtime(LocalDateTime.now());
		model.setRemark("测试测试测试分表分表分表");
		return tabrecordService.save(model);
	}
	
}

