package com.aecc.sj.oracle.controller;


import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aecc.sj.oracle.entity.Goods;
import com.aecc.sj.oracle.service.IGoodsService;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author CodeGenerator
 * @since 2020-11-11
 */
@RestController
@RequestMapping("/goods")
public class GoodsController {
	
	@Autowired
	private IGoodsService goodsService;
	
	@GetMapping("/edit")
	public boolean edit() {
		Goods good = new Goods();
		good.setGid(UUID.randomUUID().toString());
		good.setGname("测试1");
		good.setGstatus("1");
		good.setUserId("123");
		good.setId(IdWorker.getId());
		return goodsService.save(good);
	}
	
	
}

