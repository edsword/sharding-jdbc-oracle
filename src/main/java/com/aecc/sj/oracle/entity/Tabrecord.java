package com.aecc.sj.oracle.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author CodeGenerator
 * @since 2020-11-30
 */
@TableName("TABRECORD")
public class Tabrecord implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId("ID")
    private Long id;

    @TableField("TNAME")
    private String tname;

    @TableField("ADDTIME")
    private LocalDateTime addtime;

    @TableField("REMARK")
    private String remark;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTname() {
        return tname;
    }

    public void setTname(String tname) {
        this.tname = tname;
    }

    public LocalDateTime getAddtime() {
        return addtime;
    }

    public void setAddtime(LocalDateTime addtime) {
        this.addtime = addtime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "Tabrecord{" +
        "id=" + id +
        ", tname=" + tname +
        ", addtime=" + addtime +
        ", remark=" + remark +
        "}";
    }
}
