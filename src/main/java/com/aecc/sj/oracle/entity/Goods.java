package com.aecc.sj.oracle.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author CodeGenerator
 * @since 2020-11-11
 */
@TableName("GOODS")
public class Goods implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 主键id
     */
    @TableId("GID")
    private String gid;

    /**
     * 商品名称
     */
    @TableField("GNAME")
    private String gname;

    /**
     * 用户有id
     */
    @TableField("USER_ID")
    private String userId;

    /**
     * 商品状态
     */
    @TableField("GSTATUS")
    private String gstatus;

    @TableField("ID")
    private Long id;

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    public String getGname() {
        return gname;
    }

    public void setGname(String gname) {
        this.gname = gname;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getGstatus() {
        return gstatus;
    }

    public void setGstatus(String gstatus) {
        this.gstatus = gstatus;
    }
    
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
    public String toString() {
        return "Goods{" +
        "gid=" + gid +
        ", gname=" + gname +
        ", userId=" + userId +
        ", gstatus=" + gstatus +
        ", id=" + id +
        "}";
    }
}
