package com.aecc.sj.oracle;

import java.util.List;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.aecc.sj.oracle.entity.Goods;
import com.aecc.sj.oracle.service.IGoodsService;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class GoodsTest {

	@Autowired
	private IGoodsService goodsService;
	
	@Test
	public void insert(){
		for(int i=0;i<100;i++) {
			Goods good = new Goods();
			good.setGid(UUID.randomUUID().toString());
			good.setGname("测试"+i);
			good.setGstatus("1");
			good.setUserId("123");
			good.setId(IdWorker.getId());
			goodsService.save(good);
		}
	}
	
	
	public void select(){
		List<Goods> list = goodsService.list();
		System.out.println("行数："+list.size());
	}
	
}
