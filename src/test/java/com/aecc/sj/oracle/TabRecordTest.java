package com.aecc.sj.oracle;

import java.time.LocalDateTime;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.aecc.sj.oracle.entity.Tabrecord;
import com.aecc.sj.oracle.service.ITabrecordService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TabRecordTest {
	
	@Autowired
	private ITabrecordService tabrecordService;
	
	@Test
	public void insert() {
		for(int i=0;i<100;i++) {
			Tabrecord model = new Tabrecord();
//			model.setId(IdWorker.getId());
			model.setTname("测试名称"+i);
			model.setAddtime(LocalDateTime.now());
			model.setRemark("测试测试测试分表分表分表"+i);
			tabrecordService.save(model);
		}
	}
	
}
